# --------------------------------------------------------
# Pytorch FPN implementation
# Licensed under The MIT License [see LICENSE for details]
# Written by Jianwei Yang, based on code from faster R-CNN
# --------------------------------------------------------

from __future__ import absolute_import, division, print_function

import argparse
import os
import pdb
import pickle
import pprint
import sys
import time

import numpy as np
import torch
import torch.nn as nn
import torch.optim as optim
from torch.autograd import Variable
import csv
import cv2
from fpn.multibox_model.fpn.resnet import FPNResNet
from fpn.multibox_model.nms.nms_wrapper import nms, soft_nms
from fpn.multibox_model.rpn.bbox_transform import bbox_transform_inv, clip_boxes
from fpn.multibox_model.utils.config import (cfg, cfg_from_file, cfg_from_list,
                                    get_output_dir)
from fpn.multibox_model.utils.net_utils import vis_detections
from fpn.multibox_roi_data_layer.roibatchLoader import roibatchLoader
from fpn.multibox_roi_data_layer.roidb import combined_roidb
from fpn.test import validate


def parse_args():
    """
    Parse input arguments
    """
    parser = argparse.ArgumentParser(description='Train a Fast R-CNN network')
    parser.add_argument('exp_name', type=str, default=None, help='experiment name')
    parser.add_argument('--dataset', dest='dataset',
                        help='training dataset',
                        default='pascal_voc', type=str)
    parser.add_argument('--imdbval_name', dest='imdbval_name',
                        help='val imdb',
                        default='', type=str)
    # parser.add_argument('--cfg', dest='cfg_file',
    #                     help='optional config file',
    #                     default='cfgs/vgg16.yml', type=str)
    parser.add_argument('--net', dest='net',
                        help='vgg16, res50, res101, res152',
                        default='res101', type=str)
    parser.add_argument('--set', dest='set_cfgs',
                        help='set config keys', default=None,
                        nargs=argparse.REMAINDER)
    parser.add_argument('--load_dir', dest='load_dir',
                        help='directory to load models', default="output/fpn",
                        type=str)
    parser.add_argument('--cuda', dest='cuda',
                        help='whether use CUDA',
                        action='store_true')
    parser.add_argument('--ls', dest='large_scale',
                        help='whether use large imag scale',
                        action='store_true')
    parser.add_argument('--mGPUs', dest='mGPUs',
                        help='whether use multiple GPUs',
                        action='store_true')
    parser.add_argument('--score_thresh', dest='score_thresh',
                        help='treshhold for classification score',
                        default=0.05, type=float)

    parser.add_argument('--parallel_type', dest='parallel_type',
                        help='which part of model to parallel, 0: all, 1: model before roi pooling',
                        default=0, type=int)
    parser.add_argument('--checksession', dest='checksession',
                        help='checksession to load model',
                        default=1, type=int)
    parser.add_argument('--checkepoch', dest='checkepoch',
                        help='checkepoch to load network',
                        default=1, type=int)
    parser.add_argument('--vis', dest='vis',
                        help='visualization mode',
                        action='store_true')
    parser.add_argument('--soft_nms', help='whether use soft_nms', action='store_true')
    args = parser.parse_args()
    return args


def main():
    args = parse_args()

    print('Called with args:')
    print(args)

    if args.dataset == "pascal_voc":
        args.imdbval_name = "voc_2007_test"
        set_cfgs = ['ANCHOR_SCALES', '[8, 16, 32]', 'ANCHOR_RATIOS', '[0.5,1,2]']
    elif args.dataset == "pascal_voc_0712":
        args.imdbval_name = "voc_0712_test"
        set_cfgs = ['ANCHOR_SCALES', '[8, 16, 32]', 'ANCHOR_RATIOS', '[0.5,1,2]']
    elif args.dataset == "coco":
        args.imdbval_name = "coco_2014_minival"
        set_cfgs = ['ANCHOR_SCALES', '[4, 8, 16, 32]', 'ANCHOR_RATIOS', '[0.5,1,2]']
    elif args.dataset == "imagenet":
        args.imdbval_name = "imagenet_val"
        set_cfgs = ['ANCHOR_SCALES', '[8, 16, 32]', 'ANCHOR_RATIOS', '[0.5,1,2]']
    elif args.dataset == "vg":
        args.imdbval_name = "vg_150-50-50_minival"
        set_cfgs = ['ANCHOR_SCALES', '[4, 8, 16, 32]', 'ANCHOR_RATIOS', '[0.5,1,2]']
    elif 'mot_2017' in args.dataset or 'mot19_cvpr' in args.dataset:
        set_cfgs = ['ANCHOR_SCALES', '[4, 8, 16, 32]', 'ANCHOR_RATIOS', '[0.5,1,2]']
    elif 'sports_2019_train' in args.dataset or 'sports_2019_test' in args.dataset:
        set_cfgs = ['ANCHOR_SCALES', '[4, 8, 16, 32]', 'ANCHOR_RATIOS', '[0.5,1,2]']
    # elif args.dataset == "mot_2017_train":
    #     # args.imdb_name = "mot_2017_train"
    #     args.imdbval_name = "mot_2017_train"
    #     set_cfgs = ['ANCHOR_SCALES', '[4, 8, 16, 32]', 'ANCHOR_RATIOS', '[0.5,1,2]']
    # elif args.dataset == "mot_2017_small_train":
    #     # args.imdb_name = "mot_2017_small_train"
    #     args.imdbval_name = "mot_2017_small_train"
    #     set_cfgs = ['ANCHOR_SCALES', '[4, 8, 16, 32]', 'ANCHOR_RATIOS', '[0.5,1,2]']
    # elif args.dataset == "mot_2017_small_val":
    #     # args.imdb_name = "mot_2017_small_train"
    #     args.imdbval_name = "mot_2017_small_val"
    #     set_cfgs = ['ANCHOR_SCALES', '[4, 8, 16, 32]', 'ANCHOR_RATIOS', '[0.5,1,2]']
    # elif args.dataset == "mot_2017_all":
    #     # args.imdb_name = "mot_2017_small_train"
    #     args.imdbval_name = "mot_2017_all"
    #     set_cfgs = ['ANCHOR_SCALES', '[4, 8, 16, 32]', 'ANCHOR_RATIOS', '[0.5,1,2]']
    else:
        raise NotImplementedError

    input_dir = os.path.join(args.load_dir, args.net,
                             args.dataset, args.exp_name)
    if not os.path.exists(input_dir):
        raise Exception('There is no input directory for loading network from ' + input_dir)
    load_name = os.path.join(
        input_dir, f"fpn_{args.checksession}_{args.checkepoch}.pth")

    cfg_file = os.path.join(input_dir, 'config.yaml')
    cfg_from_file(cfg_file)
    cfg_from_list(set_cfgs)

    print('Using config:')
    pprint.pprint(cfg)
    np.random.seed(cfg.RNG_SEED)

    # data
    # cfg.TRAIN.USE_FLIPPED = False
    imdb, roidb, ratio_list, ratio_index = combined_roidb(args.imdbval_name, False)
    imdb.competition_mode(on=True)
    print('{:d} roidb entries'.format(len(roidb)))
    output_dir = get_output_dir(imdb, args.exp_name)
    dataset = roibatchLoader(roidb, ratio_list, ratio_index, 1,
                             imdb.num_classes, training=False, normalize=False)
    dataloader = torch.utils.data.DataLoader(dataset, batch_size=1,
                                             shuffle=False, num_workers=4,
                                             pin_memory=True)

    # network
    print("load checkpoint %s" % (load_name))

    if args.net == 'res101':
        FPN = FPNResNet(imdb.classes, 101, pretrained=False)
    elif args.net == 'res50':
        FPN = FPNResNet(imdb.classes, 50, pretrained=False)
    elif args.net == 'res152':
        FPN = FPNResNet(imdb.classes, 152, pretrained=False)
    else:
        print("Network is not defined.")
        pdb.set_trace()

    FPN.create_architecture()
    FPN.load_state_dict(torch.load(load_name)['model'])
    print('load model successfully!')

    if args.cuda:
        cfg.CUDA = True
        FPN.cuda()
    elif torch.cuda.is_available():
        print("WARNING: You have a CUDA device, so you should probably run with --cuda")

    start = time.time()

    _t = {'im_detect': time.time(), 'misc': time.time()}
    det_file = os.path.join(output_dir, 'detections.pkl')

    all_boxes = validate(FPN, dataloader, imdb, vis=args.vis,
                         cuda=args.cuda, soft_nms=args.soft_nms, score_thresh=args.score_thresh)
    # pdb.set_trace()
    boxes = all_boxes#all_boxes[1]
    boxes = boxes[1:11]
    class_0 = boxes[0]
    class_1 = boxes[1]
    class_2 = boxes[2]
    class_3 = boxes[3]
    class_4 = boxes[4]
    class_5 = boxes[5]
    class_6 = boxes[6]
    class_7 = boxes[7]
    class_8 = boxes[8]
    class_9 = boxes[9]
    final_boxes = []
    f_class_1 = []
    f_class_2 = []
    f_class_3 = []
    f_class_4 = []
    f_class_5 = []
    f_class_6 = []
    f_class_7 = []
    f_class_8 = []
    f_class_9 = []
    f_class_10 = []
    for c0_frame,c1_frame,c2_frame,c3_frame,c4_frame,c5_frame,c6_frame,c7_frame,c8_frame,c9_frame in zip(class_0,class_1,class_2,class_3,class_4,class_5,class_6,class_7,class_8,class_9):
        a_frame_box = np.concatenate((c0_frame,c1_frame,c2_frame,c3_frame,c4_frame,c5_frame,c6_frame,c7_frame,c8_frame,c9_frame),axis=0)
        box_and_scores = a_frame_box[:,0:5]
        classes = torch.from_numpy(a_frame_box[:,5]).float().cuda()
        box_and_scores = torch.from_numpy(box_and_scores).float().cuda()
        keep = nms(box_and_scores,.30)
        # pdb.set_trace()
        box_and_scores = box_and_scores[keep.view(-1).long()]
        # print(len(classes))
        classes = classes[keep.view(-1).long()]
        # print(len(classes))

        classes = classes.view(-1,1)
        # pdb.set_trace()
        
        frame_box = torch.cat([box_and_scores,classes],dim=1)
        frame_box = frame_box.cpu().numpy()
        c1=[]
        c2=[]
        c3=[]
        c4=[]
        c5=[]
        c6=[]
        c7=[]
        c8=[]
        c9=[]
        c10=[]
        for a_box in enumerate(frame_box):
            # pdb.set_trace()
            # a_box = a_box.cpu().numpy()
            box_cls=a_box[1][5]
            if int(box_cls)==1:
               c1.append(a_box[1]) 
            if int(box_cls)==2:
               c2.append(a_box[1]) 
            if int(box_cls)==3:
               c3.append(a_box[1]) 
            if int(box_cls)==4:
               c4.append(a_box[1]) 
            if int(box_cls)==5:
               c5.append(a_box[1]) 
            if int(box_cls)==6:
               c6.append(a_box[1]) 
            if int(box_cls)==7:
               c7.append(a_box[1])
            if int(box_cls)==8:
               c8.append(a_box[1])
            if int(box_cls)==9:
               c9.append(a_box[1]) 
            if int(box_cls)==10:
               c10.append(a_box[1])

        # pdb.set_trace()
        if c1!=[]:
            c1 = np.stack(c1)
        if c2!=[]:
            c2 = np.stack(c2)
        if c3!=[]:
            c3 = np.stack(c3)
        if c4!=[]:
            c4 = np.stack(c4)
        if c5!=[]:
            c5 = np.stack(c5)
        if c6!=[]:
            c6 = np.stack(c6)
        if c7!=[]:
            c7 = np.stack(c7)
        if c8!=[]:
            c8 = np.stack(c8)
        if c9!=[]:
            c9 = np.stack(c9)
        if c10!=[]:
            c10 = np.stack(c10)

        f_class_1.append(c1)
        f_class_2.append(c2)
        f_class_3.append(c3)
        f_class_4.append(c4)
        f_class_5.append(c5)
        f_class_6.append(c6)
        f_class_7.append(c7)
        f_class_8.append(c8)
        f_class_9.append(c9)
        f_class_10.append(c10)

        final_boxes.append(frame_box)
    # for a_class_box in boxes:
    evaluation_boxes = []
    evaluation_boxes.append(all_boxes[0])
    evaluation_boxes.append(f_class_1)
    evaluation_boxes.append(f_class_2)
    evaluation_boxes.append(f_class_3)
    evaluation_boxes.append(f_class_4)
    evaluation_boxes.append(f_class_5)
    evaluation_boxes.append(f_class_6)
    evaluation_boxes.append(f_class_7)
    evaluation_boxes.append(f_class_8)
    evaluation_boxes.append(f_class_9)
    evaluation_boxes.append(f_class_10)
    # for a_class_box in boxes:
    for ids, a_frame_box in enumerate(final_boxes):
        # pdb.set_trace()
        for a_box in a_frame_box:
            # a_box = a_box.cpu().numpy()
            # print(a_box)
            if a_box!=[]:
                with open('/media/sohel/HDD/tracking_wo_bnw/data/sports/test/second_half_netball/det/multidet.txt', 'a+') as csvfile:
                    writer = csv.writer(csvfile, delimiter=',',quotechar='|', quoting=csv.QUOTE_MINIMAL)
                    writer.writerow([4000+ids+1,-1,a_box[0],a_box[1],a_box[2]-a_box[0],a_box[3]-a_box[1],a_box[4],a_box[5],-1,-1])
                csvfile.close()            
    # pdb.set_trace()
    # pdb.set_trace()
    # for ids,a_frame_box in enumerate(boxes):
    #     # print("g")
    #     for a_box in a_frame_box:
            
    #         with open('/media/sohel/HDD/tracking_wo_bnw/data/sports/test/second_half_netball/det/multidet.txt', 'a+') as csvfile:
    #             writer = csv.writer(csvfile, delimiter=',',quotechar='|', quoting=csv.QUOTE_MINIMAL)
    #             writer.writerow([4000+ids+1,-1,a_box[0],a_box[1],a_box[2]-a_box[0],a_box[3]-a_box[1],a_box[4],a_box[5],-1,-1])
    #         csvfile.close()

    # pdb.set_trace()
    print('Evaluating detections')
    imdb.evaluate_detections(evaluation_boxes, output_dir)

    with open(det_file, 'wb') as f:
        pickle.dump(evaluation_boxes, f, pickle.HIGHEST_PROTOCOL)

    end = time.time()
    print("test time: %0.4fs" % (end - start))


if __name__ == '__main__':
    main()
