FROM nvidia/cuda:9.0-base-ubuntu16.04
FROM continuumio/miniconda3

#export PATH=
#ENV PATH "$PATH:/new/path"
RUN export PATH=/usr/local/cuda-9.0/bin${PATH:+:${PATH}}$ 
RUN export LD_LIBRARY_PATH=/usr/local/cuda-9.0/lib64${LD_LIBRARY_PATH:+:${LD_LIBRARY_PATH}}
#RUN /bin/bash -c "source .bashrc"


#ENV PATH=/opt/pgi/linux86-64/18.4/bin:$PATH
#ENV LD_LIBRARY_PATH=/opt/pgi/linux86-64/18.4/lib:$LD_LIBRARY_PATH
#RUN touch mylibpath && \
#    touch mybinpath && \
#    echo $PATH >> mybinpath && \
#    echo $LD_LIBRARY_PATH >> mylibpath

#ENV PATH /home/$USER/cuda-9.0/bin:"$PATH"
# Create a working directory
RUN mkdir /app
WORKDIR /app


COPY requirements.txt .
# Create the environment:
COPY environment.yml .

RUN conda env create -f environment.yml

# Activate the environment, and make sure it's activated:
SHELL ["conda", "run", "-n", "bnw-tracking", "/bin/bash", "-c"]
COPY src/frcnn .
COPY src/fpn .
COPY . .
RUN pip install -e src/frcnn
RUN pip install -e src/fpn
RUN pip install -e .

#RUN mkdir -p /scripts
#COPY src/fpn/fpn/make.sh /scripts

#COPY src/fpn/fpn /app
#WORKDIR /app
#ADD src/fpn/fpn/make.sh /
#RUN /app/make.sh

RUN mkdir -p /fpn_scripts
COPY src/fpn/fpn /fpn_scripts
RUN chmod +x /fpn_scripts/make.sh

RUN mkdir -p /scripts
COPY src/frcnn/frcnn /scripts
RUN chmod +x /scripts/make.sh
#WORKDIR /scripts
#RUN make.sh

WORKDIR /app

# Create a non-root user and switch to it
#RUN adduser --disabled-password --gecos '' --shell /bin/bash user \
# && chown -R user:user /app
#RUN echo "user ALL=(ALL) NOPASSWD:ALL" > /etc/sudoers.d/90-user
#USER user

# All users can use /home/user as their home directory
#ENV HOME=/home/user
#RUN chmod 777 /home/user

# Create /data directory so that a container can be run without volumes mounted
#RUN sudo mkdir /data && sudo chown user:user /data
# Copy source code into the image
#COPY --chown=user:user . /app

# Create /data directory so that a container can be run without volumes mounted
#COPY --chown=user:user . /app
# Set the default command to python3
CMD ["python3"]

#ENTRYPOINT ["conda", "run", "-n", "bnw-tracking", "python3", "test.py"]