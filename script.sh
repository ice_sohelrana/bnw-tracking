IMAGE="registry.dl.cs.latrobe.edu.au/sohel/bnw-tracking"
# 1. Build image
docker build . -t "$IMAGE"
# 2. Run on the local just after build
docker run \
--runtime=nvidia -it \
--rm "$IMAGE" python3 test.py
#sports_2019_train --cuda --net res101 --dataset sports_2019_train --imdbval_name sports_2019_test --checkepoch 37 \
# --volume=/home/sohel/sohel_experiment/data/mot17_bnw:/data \

# --dataset "mot_2017_train" \

#--volume=/home/sohel/sohel_experiment/tracking_wo_bnw/output/fpn/res101/pascal_voc_0712/v2:/precheckpoint \
