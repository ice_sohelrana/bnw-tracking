# --------------------------------------------------------
# Fast R-CNN
# Copyright (c) 2015 Microsoft
# Licensed under The MIT License [see LICENSE for details]
# Written by Ross Girshick
# --------------------------------------------------------

import configparser
import csv
import os
import os.path as osp
import pickle

# import PIL
import numpy as np
import scipy
from itertools import combinations
from ..multibox_model.utils.config import cfg
from .imdb import imdb
import pdb

class MULTIDET_SPORTS(imdb):
    """ Data class for the Multiple Object Tracking Dataset

    In the MOT dataset the different images are stored in folders belonging to the
    sequences. In every sequence the image index starts at 000001.jpg again. Therefore
    the image name can't be used directly as index and has to be mapped to a unique positive
    integer.

    Attributes:
        _image_index: List of mapped unique indexes of each image that will be used in this set

    """

    def __init__(self, image_set, year):
        imdb.__init__(self, 'multidet_sports_' + year + '_' + image_set)

        self._year = year
        self._image_set = image_set
        self._sports_dir = os.path.join(
            cfg.DATA_DIR, 'sports')
        self._mot_train_dir = os.path.join(self._sports_dir, 'train')
        self._mot_test_dir = os.path.join(self._sports_dir, 'test')
        self._roidb_handler = self.gt_roidb

        # self._classes = ('__background__',  # always index 0
        #                  'multiperson')
        self._classes = ('__background__',  # always index 0
                         '1','2','3')#'1','2','3','4')#,'6','7','8','9','10')
        #self._num_classes = len(self._classes)

        self._train_folders = ['first_half_netball']
        self._test_folders = ['second_half_netball']
        if 'seq' in self._image_set:
            split_num = int(self._image_set[-1])
            if 'train' in self._image_set:
                self._train_folders.pop(len(self._train_folders) - split_num)
            else:
                self._train_folders = [self._train_folders.pop(len(self._train_folders) - split_num)]
        assert os.path.exists(self._sports_dir), \
            'Path does not exist: {}'.format(self._sports_dir)
        assert os.path.exists(self._mot_train_dir), \
            'Path does not exist: {}'.format(self._mot_train_dir)
        assert os.path.exists(self._mot_test_dir), \
            'Path does not exist: {}'.format(self._mot_test_dir)

        self._index_to_path = {}
        self._index_to_width = {}
        self._index_to_height = {}

        counter = 0
        frame_train_set = []
        frame_val_set = []

        for f in self._train_folders:
            path = os.path.join(self._mot_train_dir, f)
            config_file = os.path.join(path, 'seqinfo.ini')

            assert os.path.exists(config_file), \
                'Path does not exist: {}'.format(config_file)

            config = configparser.ConfigParser()
            config.read(config_file)
            seqLength = int(config['Sequence']['seqLength'])
            imWidth = int(config['Sequence']['imWidth'])
            imHeight = int(config['Sequence']['imHeight'])
            imExt = config['Sequence']['imExt']
            imDir = config['Sequence']['imDir']

            _imDir = os.path.join(path, imDir)

            for i in range(1, seqLength+1):
                im_path = os.path.join(_imDir, ("{:08d}"+imExt).format(i))
                assert os.path.exists(im_path), \
                    'Path does not exist: {}'.format(im_path)
                self._index_to_path[i+counter] = im_path
                self._index_to_width[i+counter] = imWidth
                self._index_to_height[i+counter] = imHeight
                if i <= seqLength*1.0: #.5
                    frame_train_set.append(i+counter)
                # if i > seqLength*0.75: #.75
                #     frame_val_set.append(i+counter)

            counter += seqLength

        self._train_counter = counter

        for f in self._test_folders:
            path = os.path.join(self._mot_test_dir, f)
            config_file = os.path.join(path, 'seqinfo.ini')

            assert os.path.exists(config_file), \
                'Path does not exist: {}'.format(config_file)

            config = configparser.ConfigParser()
            config.read(config_file)
            seqLength = int(config['Sequence']['seqLength'])
            imWidth = int(config['Sequence']['imWidth'])
            imHeight = int(config['Sequence']['imHeight'])
            imExt = config['Sequence']['imExt']
            imDir = config['Sequence']['imDir']

            _imDir = os.path.join(path, imDir)

            for i in range(1, seqLength+1):
                im_path = os.path.join(_imDir, ("{:08d}"+imExt).format(i)) ### change here 4000+i
                assert os.path.exists(im_path), \
                    'Path does not exist: {}'.format(im_path)
                self._index_to_path[i+counter] = im_path
                self._index_to_width[i+counter] = imWidth
                self._index_to_height[i+counter] = imHeight

            counter += seqLength
        train_set = [i for i in range(1, self._train_counter+1)]
        test_set = [i for i in range(self._train_counter+1, counter+1)]
        self._train_counter=counter
        all_set = [i for i in range(1, counter+1)]

        if self._image_set == "train" or "seq" in self._image_set:
            self._image_index = train_set
        elif self._image_set == "frame_train":
            self._image_index = frame_train_set
        elif self._image_set == "frame_val":
            self._image_index = frame_val_set
        elif self._image_set == "test":
            self._image_index = test_set
        elif self._image_set == "all":
            self._image_index = all_set
        # person_gt_ids = []
		# with open(self.tracks_gt_path, 'r') as csvfile:
		# 	csv_read = csv.reader(csvfile, delimiter = ',')
		# 	for row in csv_read:
		# 		if self.video_type=="second_half":
		# 			frame_url = self.detection_image_paths[int(row[0])-1-4000] #int(row[0])-1-4000
		# 		else:
		# 			frame_url = self.detection_image_paths[int(row[0])-1] #int(row[0])-1-4000 #int(row[0])-1-4000
		# 		person_id_frame_no = {"frame_number": row[0], "person_id": row[1], "x":int(float(row[2])),"y":int(float(row[3])),"width":int(float(row[4])),"height":int(float(row[5])),"url": frame_url} #person,person_id,frame_number
		# 		object_gt_list.append(person_id_frame_no) 
    def image_path_at(self, i):
        """
        Return the absolute path to image i (!= index, which begins at posivitve value and can
        have gaps) in the image sequence.
        0 based index
        """
        return self._index_to_path[self._image_index[i]]

    def image_id_at(self, i):
        """
        Return the absolute path to image i in the image sequence.
        """
        return i

    def gt_roidb(self):
        """
        Return the database of ground-truth regions of interest.

        This function loads/saves from/to a cache file to speed up future calls.
        """
        cache_file = os.path.join(self.cache_path, self.name + '_gt_roidb.pkl')
        
        if os.path.exists(cache_file):
            with open(cache_file, 'rb') as fid:
                roidb = pickle.load(fid)
            print('{} gt roidb loaded from {}'.format(self.name, cache_file))
            return roidb
        # all_seg_areas = []
        # for index in self.image_index:
        #     seg_areas = self.get_seg_areas(index)
        #     all_seg_areas = all_seg_areas + seg_areas
        # max_area = max(all_seg_areas)
        # pdb.set_trace()
        gt_roidb = [self._load_mot_annotation(index)
                    for index in self.image_index]
        # seg_areas  = []
        # for a_gt in twoperson_gt_roidb:
        #     gt_seg_areas = a_gt['seg_areas']
        #     for a_gt_seg_area in gt_seg_areas:
        #         seg_areas.append(a_gt_seg_area)
        # max_seg_areas = max(seg_areas)
        # # pdb.set_trace()
        # threeperson_gt_roidb = [self._load_mot_annotation(index,3,max_seg_areas)
        #             for index in self.image_index]
        # maximum_seg_val = max(seg_areas)
        # seg_areas = [a_areas['seg_areas'] for a_areas in gt_roidb]
        # twobox_gt_roidb = [self._load_mot_annotation(index)
        #             for index in self.image_index]

        # gt_roidb = twoperson_gt_roidb + threeperson_gt_roidb        
        # pdb.set_trace()
        # multigt_roidb = []
        # for inds,a_gt_roidb in enumerate(gt_roidb):
        #     total_gt_roidb = len(a_gt_roidb["boxes"])
        #     gt_indexes = [a for a in range(total_gt_roidb)]
        #     indexes = combinations(gt_indexes,2)
        #     bbs = []
        #     seg_areas = []
        #     visibilities = []
        #     classes = []
        #     for a_index in list(indexes):
        #         iou = self.bb_intersection_over_union(a_gt_roidb["boxes"][a_index[0]],a_gt_roidb["boxes"][a_index[1]])
        #         if iou>.10:
        #             x1 = min(a_gt_roidb["boxes"][a_index[0]][0],a_gt_roidb["boxes"][a_index[1]][0])
        #             y1 = min(a_gt_roidb["boxes"][a_index[0]][1],a_gt_roidb["boxes"][a_index[1]][1])
        #             x2 = max(a_gt_roidb["boxes"][a_index[0]][2],a_gt_roidb["boxes"][a_index[1]][2])
        #             y2 = max(a_gt_roidb["boxes"][a_index[0]][3],a_gt_roidb["boxes"][a_index[1]][3])
        #             bb = np.array([x1,y1,x2,y2])
        #             seg_area = float((x2 - x1 + 1) * (y2 - y1 + 1))
        #             bbs.append(bb)
        #             seg_areas.append(seg_area)
        #             visibilities.append(1)
        #             classes.append(1)
        #     if len(bbs)>0:
        #         bbs = np.stack(bbs)
        #         seg_areas = np.stack(seg_areas)
        #         visibilities = np.stack(visibilities)
        #         classes = np.stack(classes)
        #         gt_roidb[inds] = {"boxes":bbs,'gt_classes':classes,'gt_overlaps':a_gt_roidb["gt_overlaps"],'flipped': False,'seg_areas':seg_areas,"visibilities":visibilities}
        #     else:
        #         gt_roidb[inds] = {"boxes":[],'gt_classes':[],'gt_overlaps':[],'flipped': False,'seg_areas':[],"visibilities":[]}
                # multigt_roidb.append()
        with open(cache_file, 'wb') as fid:
            pickle.dump(gt_roidb, fid, pickle.HIGHEST_PROTOCOL)
        print('wrote gt roidb to {}'.format(cache_file))

        return gt_roidb
    def get_seg_areas(self,index):
        if index > self._train_counter:
            boxes = np.zeros((0, 4), dtype=np.uint16)
            gt_classes = np.zeros((0), dtype=np.int32)
            overlaps = np.zeros((0, self.num_classes), dtype=np.float32)
            overlaps = scipy.sparse.csr_matrix(overlaps)
            seg_areas = np.zeros((0), dtype=np.float32)
            visibilities = np.zeros((0), dtype=np.float32)

            return {'boxes': boxes,
                    'gt_classes': gt_classes,
                    #'gt_ishard': ishards,
                    'gt_overlaps': overlaps,
                    'flipped': False,
                    'seg_areas': seg_areas,
                    'visibilities': visibilities}
        image_pth = self._index_to_path[index]
        file_index = int(os.path.basename(image_pth).split('.')[0])

        gt_file = os.path.join(os.path.dirname(
            os.path.dirname(image_pth)), 'gt', 'gt.txt') #gt gt.txt

        assert os.path.exists(gt_file), \
            'Path does not exist: {}'.format(gt_file)
        
        bounding_boxes = []
        with open(gt_file, "r") as inf:
            reader = csv.reader(inf, delimiter=',')
            for row in reader:
                if int(row[0]) == file_index and int(row[6]) == 1 and int(row[7]) == 1 and float(row[8]) >= 0.25:
                    bb = {}
                    bb['bb_left'] = int(float(row[2])) #int(row[2])
                    bb['bb_top'] = int(float(row[3])) #int(row[3])
                    bb['bb_width'] = int(float(row[4])) #int(row[4])
                    bb['bb_height'] = int(float(row[5])) #int(row[5])
                    bb['visibility'] = 1 #float(row[8])
                    bounding_boxes.append(bb)

        num_objs = len(bounding_boxes)
        boxes = np.zeros((num_objs, 4), dtype=np.int16)
        gt_classes = np.zeros((num_objs), dtype=np.int32)
        overlaps = np.zeros((num_objs, self.num_classes), dtype=np.float32)
        # "Seg" area for pascal is just the box area
        seg_areas = np.zeros((num_objs), dtype=np.float32)
        #ishards = np.zeros((num_objs), dtype=np.int32)
        visibilities = np.zeros((num_objs), dtype=np.float32)

        for i, bb in enumerate(bounding_boxes):
            # Make pixel indexes 0-based, should already be 0-based (or not)
            x1 = bb['bb_left'] - 1
            y1 = bb['bb_top'] - 1
            # This -1 accounts for the width (width of 1 x1=x2)
            x2 = x1 + bb['bb_width'] - 1
            y2 = y1 + bb['bb_height'] - 1

            boxes[i, :] = [x1, y1, x2, y2]
            # Class is always pedestrian
            gt_classes[i] = 1
            seg_areas[i] = float((x2 - x1 + 1) * (y2 - y1 + 1))
            #ishards[i] = 0
            overlaps[i][1] = 1.0
            visibilities[i] = bb['visibility']
        total_boxes = len(boxes)
        gt_indexes = [a for a in range(total_boxes)]
        indexes = combinations(gt_indexes,2)                       
        seg_areas = []
        for a_index in list(indexes):
            iou = self.bb_intersection_over_union(boxes[a_index[0]],boxes[a_index[1]])
            if iou>=.25:
                x1 = min(boxes[a_index[0]][0],boxes[a_index[1]][0])
                y1 = min(boxes[a_index[0]][1],boxes[a_index[1]][1])
                x2 = max(boxes[a_index[0]][2],boxes[a_index[1]][2])
                y2 = max(boxes[a_index[0]][3],boxes[a_index[1]][3])
                seg_area = float((x2 - x1 + 1) * (y2 - y1 + 1))
                seg_areas.append(seg_area)
        return seg_areas

    def _load_mot_annotation(self, index):
        """
        Loads the bounding boxes from the corresponding gt.txt files
        structure of a row in this file: <frame>, <id>, <bb left>, <bb top>, <bb width>,
        <bb height>, <conf>, <class>, <visibility>

        id: Id of the corresponding object (important for tracking not detection)
        conf: Confidence, only considering bb if this is 1
        class: 1 is class for pedestrian
        visibility: Only consider if this is > 0.25

        If image is from test set return empy elements
        """

        if index > self._train_counter:
            boxes = np.zeros((0, 4), dtype=np.uint16)
            gt_classes = np.zeros((0), dtype=np.int32)
            overlaps = np.zeros((0, self.num_classes), dtype=np.float32)
            overlaps = scipy.sparse.csr_matrix(overlaps)
            seg_areas = np.zeros((0), dtype=np.float32)
            visibilities = np.zeros((0), dtype=np.float32)

            return {'boxes': boxes,
                    'gt_classes': gt_classes,
                    #'gt_ishard': ishards,
                    'gt_overlaps': overlaps,
                    'flipped': False,
                    'seg_areas': seg_areas,
                    'visibilities': visibilities}

        image_pth = self._index_to_path[index]
        file_index = int(os.path.basename(image_pth).split('.')[0])

        gt_file = os.path.join(os.path.dirname(
            os.path.dirname(image_pth)), 'gt', 'gt.txt') #gt gt.txt

        assert os.path.exists(gt_file), \
            'Path does not exist: {}'.format(gt_file)
        
        bounding_boxes = []

        with open(gt_file, "r") as inf:
            reader = csv.reader(inf, delimiter=',')
            for row in reader:
                if int(row[0]) == file_index and int(row[6]) == 1 and int(row[7]) == 1 and float(row[8]) >= 0.25:
                    bb = {}
                    bb['bb_left'] = int(float(row[2])) #int(row[2])
                    bb['bb_top'] = int(float(row[3])) #int(row[3])
                    bb['bb_width'] = int(float(row[4])) #int(row[4])
                    bb['bb_height'] = int(float(row[5])) #int(row[5])
                    bb['visibility'] = 1 #float(row[8])

                    # Check if bb is inside the image
                    #if bb['bb_left'] > 0 and bb['bb_top'] > 0 \
                    #    and bb['bb_left']+bb['bb_width']-1 <= self._index_to_width[index] \
                    #    and bb['bb_top']+bb['bb_height']-1 <= self._index_to_height[index]:

                    # Now crop bb that are outside of image
                    #if bb['bb_left'] <= 0:
                    #    bb['bb_left'] = 1
                    #if bb['bb_top'] <= 0:
                    #    bb['bb_top'] = 1
                    #if bb['bb_left']+bb['bb_width']-1 > self._index_to_width[index]:
                    #    bb['bb_width'] = self._index_to_width[index] - bb['bb_left'] + 1
                    #if bb['bb_top']+bb['bb_height']-1 > self._index_to_height[index]:
                    #    bb['bb_height'] = self._index_to_height[index] - bb['bb_top'] + 1

                    bounding_boxes.append(bb)

        num_objs = len(bounding_boxes)
        #boxes = np.zeros((num_objs, 4), dtype=np.uint16)
        boxes = np.zeros((num_objs, 4), dtype=np.int16)
        gt_classes = np.zeros((num_objs), dtype=np.int32)
        overlaps = np.zeros((num_objs, self.num_classes), dtype=np.float32)
        # "Seg" area for pascal is just the box area
        seg_areas = np.zeros((num_objs), dtype=np.float32)
        #ishards = np.zeros((num_objs), dtype=np.int32)
        visibilities = np.zeros((num_objs), dtype=np.float32)

        for i, bb in enumerate(bounding_boxes):
            # Make pixel indexes 0-based, should already be 0-based (or not)
            x1 = bb['bb_left'] - 1
            y1 = bb['bb_top'] - 1
            # This -1 accounts for the width (width of 1 x1=x2)
            x2 = x1 + bb['bb_width'] - 1
            y2 = y1 + bb['bb_height'] - 1

            boxes[i, :] = [x1, y1, x2, y2]
            # Class is always pedestrian
            gt_classes[i] = 1
            seg_areas[i] = float((x2 - x1 + 1) * (y2 - y1 + 1))
            #ishards[i] = 0
            overlaps[i][1] = 1.0
            visibilities[i] = bb['visibility']
        total_boxes = len(boxes)
        gt_indexes = [a for a in range(total_boxes)]
        # indexes = combinations(gt_indexes,len(gt_indexes)-1)
        bbs = []
        seg_areas = []
        visibilities = []
        classes = []
        overlaps = []
        occluded_boxes = []
        # pdb.set_trace()
        
        for a_gt_index in gt_indexes:
            # pdb.set_trace()
            other_index = list(set(gt_indexes)-set([a_gt_index]))
            n_overlapped_box = 1
            ious = []
            # other_boxes = []
            other_boxes = np.zeros((5, 4), dtype=np.int16)
            max_iou=0.0
            temp_other_index1 = None
            temp_other_index2 = None
            extreme_number=True
            for ids,a_other_index in enumerate(other_index):
                iou = self.bb_intersection_over_union(boxes[a_gt_index],boxes[a_other_index])
                if iou>.10:
                    if temp_other_index1 != None and temp_other_index2 != None:
                        iou1 = self.bb_intersection_over_union(boxes[temp_other_index1],boxes[temp_other_index2])
                        iou2 = self.bb_intersection_over_union(boxes[a_other_index],boxes[temp_other_index1])
                        iou3 = self.bb_intersection_over_union(boxes[a_other_index],boxes[temp_other_index2])
                        iou4 = self.bb_intersection_over_union(boxes[a_gt_index],boxes[temp_other_index1])
                        iou5 = self.bb_intersection_over_union(boxes[a_gt_index],boxes[temp_other_index2])
                        if iou1>0.0 and iou2>0 and iou3>0 and iou4>0.0 and iou>0.0 and extreme_number==True:
                            x1 = max(boxes[a_gt_index][0],boxes[a_other_index][0],boxes[temp_other_index1][0],boxes[temp_other_index2][0])
                            y1 = max(boxes[a_gt_index][1],boxes[a_other_index][1],boxes[temp_other_index1][1],boxes[temp_other_index2][1])
                            x2 = min(boxes[a_gt_index][2],boxes[a_other_index][2],boxes[temp_other_index1][2],boxes[temp_other_index2][2])
                            y2 = min(boxes[a_gt_index][3],boxes[a_other_index][3],boxes[temp_other_index1][3],boxes[temp_other_index2][3])
                            bb = np.array([x1,y1,x2,y2])
                            seg_area = float((bb[2] - bb[0] + 1) * (bb[3] - bb[1] + 1))
                            bbs.append(bb)
                            seg_areas.append(seg_area)
                            visibilities.append(1)
                            # if max_iou>=0.20:
                            classes.append(3)
                            overlaps.append(np.array([0.0,1.0]))
                            extreme_number=False
                            n_overlapped_box=n_overlapped_box+1
                            # temp_gt_index1 = a_gt_index
                if iou>.10:
                    if temp_other_index1 != None:
                        iou = self.bb_intersection_over_union(boxes[temp_other_index1],boxes[a_other_index])
                        if iou>0.0:
                            x1 = max(boxes[a_gt_index][0],boxes[a_other_index][0],boxes[temp_other_index1][0])
                            y1 = max(boxes[a_gt_index][1],boxes[a_other_index][1],boxes[temp_other_index1][1])
                            x2 = min(boxes[a_gt_index][2],boxes[a_other_index][2],boxes[temp_other_index1][2])
                            y2 = min(boxes[a_gt_index][3],boxes[a_other_index][3],boxes[temp_other_index1][3])
                            bb = np.array([x1,y1,x2,y2])
                            seg_area = float((bb[2] - bb[0] + 1) * (bb[3] - bb[1] + 1))
                            bbs.append(bb)
                            seg_areas.append(seg_area)
                            visibilities.append(1)
                            # if max_iou>=0.20:
                            classes.append(2)
                            overlaps.append(np.array([0.0,1.0]))
                            n_overlapped_box=n_overlapped_box+1
                            # temp_gt_index1 = a_gt_index
                            temp_other_index2 = a_other_index
                if iou>.10:
                    x1 = max(boxes[a_gt_index][0],boxes[a_other_index][0])
                    y1 = max(boxes[a_gt_index][1],boxes[a_other_index][1])
                    x2 = min(boxes[a_gt_index][2],boxes[a_other_index][2])
                    y2 = min(boxes[a_gt_index][3],boxes[a_other_index][3])
                    bb = np.array([x1,y1,x2,y2])
                    seg_area = float((bb[2] - bb[0] + 1) * (bb[3] - bb[1] + 1))
                    bbs.append(bb)
                    seg_areas.append(seg_area)
                    visibilities.append(1)
                    # if max_iou>=0.20:
                    classes.append(1)
                    n_overlapped_box=n_overlapped_box+1
                    overlaps.append(np.array([0.0,1.0]))
                    # temp_gt_index1 = a_gt_index
                    temp_other_index1 = a_other_index

                # print(iou)
                # if iou>=max_iou:
                #     max_iou=iou
                # ious.append(iou)
                # ocl_index=0
                # if iou>0.0:
                #     n_overlapped_box=n_overlapped_box+1
                #     # other_boxes[ocl_index] = boxes[a_other_index]
                #     # ocl_index=ocl_index+1
                #     # pdb.set_trace()
                #     # other_boxes.append(a_other_box)
            # bb = np.array(boxes[a_gt_index])
            # seg_area = float((bb[2] - bb[0] + 1) * (bb[3] - bb[1] + 1))
            # bbs.append(bb)
            # seg_areas.append(seg_area)
            # visibilities.append(1)
            # # if max_iou>=0.20:
            # classes.append(1+max_iou)
            # # else:
            #     # classes.append(1)
            # overlaps.append(np.array([0.0,1.0]))
            # if n_overlapped_box<=2:
	        #     bb = np.array(boxes[a_gt_index])
	        #     seg_area = float((bb[2] - bb[0] + 1) * (bb[3] - bb[1] + 1))
	        #     bbs.append(bb)
	        #     seg_areas.append(seg_area)
	        #     visibilities.append(1)
	        #     classes.append(n_overlapped_box)
	        #     overlaps.append(np.array([0.0,1.0]))
	            # other_boxes = np.stack(other_boxes)
            # occluded_boxes.append(other_boxes)
        # print(classes)
        # pdb.set_trace()
        if len(bbs)>0:
            boxes = np.stack(bbs)
            seg_areas = np.stack(seg_areas)
            visibilities = np.stack(visibilities)
            gt_classes = np.stack(classes)
            overlaps = np.stack(overlaps)
            # occluded_boxes = np.stack(occluded_boxes)
        # pdb.set_trace()
        else:
            boxes = np.zeros((0, 4), dtype=np.int16)
            gt_classes = np.zeros((0), dtype=np.int32)
            overlaps = np.zeros((0, self.num_classes), dtype=np.float32)
            # "Seg" area for pascal is just the box area
            seg_areas = np.zeros((0), dtype=np.float32)
            #ishards = np.zeros((num_objs), dtype=np.int32)
            visibilities = np.zeros((0), dtype=np.float32)
            # occluded_boxes = np.zeros((0,5,4),dtype=np.int16)
            # occluded_boxes
        overlaps = scipy.sparse.csr_matrix(overlaps)
        return {'boxes': boxes,
                'gt_classes': gt_classes,
                #'gt_ishard': ishards,
                'gt_overlaps': overlaps,
                'flipped': False,
                'seg_areas': seg_areas,
                'visibilities': visibilities,
                }
    def _load_mot_annotation_multibox(self, index):
        """
        Loads the bounding boxes from the corresponding gt.txt files
        structure of a row in this file: <frame>, <id>, <bb left>, <bb top>, <bb width>,
        <bb height>, <conf>, <class>, <visibility>

        id: Id of the corresponding object (important for tracking not detection)
        conf: Confidence, only considering bb if this is 1
        class: 1 is class for pedestrian
        visibility: Only consider if this is > 0.25

        If image is from test set return empy elements
        """

        if index > self._train_counter:
            boxes = np.zeros((0, 4), dtype=np.uint16)
            gt_classes = np.zeros((0), dtype=np.int32)
            overlaps = np.zeros((0, self.num_classes), dtype=np.float32)
            overlaps = scipy.sparse.csr_matrix(overlaps)
            seg_areas = np.zeros((0), dtype=np.float32)
            visibilities = np.zeros((0), dtype=np.float32)

            return {'boxes': boxes,
                    'gt_classes': gt_classes,
                    #'gt_ishard': ishards,
                    'gt_overlaps': overlaps,
                    'flipped': False,
                    'seg_areas': seg_areas,
                    'visibilities': visibilities}

        image_pth = self._index_to_path[index]
        file_index = int(os.path.basename(image_pth).split('.')[0])

        gt_file = os.path.join(os.path.dirname(
            os.path.dirname(image_pth)), 'gt', 'gt.txt') #gt gt.txt

        assert os.path.exists(gt_file), \
            'Path does not exist: {}'.format(gt_file)
        
        bounding_boxes = []

        with open(gt_file, "r") as inf:
            reader = csv.reader(inf, delimiter=',')
            for row in reader:
                if int(row[0]) == file_index and int(row[6]) == 1 and int(row[7]) == 1 and float(row[8]) >= 0.25:
                    bb = {}
                    bb['bb_left'] = int(float(row[2])) #int(row[2])
                    bb['bb_top'] = int(float(row[3])) #int(row[3])
                    bb['bb_width'] = int(float(row[4])) #int(row[4])
                    bb['bb_height'] = int(float(row[5])) #int(row[5])
                    bb['visibility'] = 1 #float(row[8])

                    # Check if bb is inside the image
                    #if bb['bb_left'] > 0 and bb['bb_top'] > 0 \
                    #    and bb['bb_left']+bb['bb_width']-1 <= self._index_to_width[index] \
                    #    and bb['bb_top']+bb['bb_height']-1 <= self._index_to_height[index]:

                    # Now crop bb that are outside of image
                    #if bb['bb_left'] <= 0:
                    #    bb['bb_left'] = 1
                    #if bb['bb_top'] <= 0:
                    #    bb['bb_top'] = 1
                    #if bb['bb_left']+bb['bb_width']-1 > self._index_to_width[index]:
                    #    bb['bb_width'] = self._index_to_width[index] - bb['bb_left'] + 1
                    #if bb['bb_top']+bb['bb_height']-1 > self._index_to_height[index]:
                    #    bb['bb_height'] = self._index_to_height[index] - bb['bb_top'] + 1

                    bounding_boxes.append(bb)

        num_objs = len(bounding_boxes)

        #boxes = np.zeros((num_objs, 4), dtype=np.uint16)
        boxes = np.zeros((num_objs, 4), dtype=np.int16)
        gt_classes = np.zeros((num_objs), dtype=np.int32)
        overlaps = np.zeros((num_objs, self.num_classes), dtype=np.float32)
        # "Seg" area for pascal is just the box area
        seg_areas = np.zeros((num_objs), dtype=np.float32)
        #ishards = np.zeros((num_objs), dtype=np.int32)
        visibilities = np.zeros((num_objs), dtype=np.float32)

        for i, bb in enumerate(bounding_boxes):
            # Make pixel indexes 0-based, should already be 0-based (or not)
            x1 = bb['bb_left'] - 1
            y1 = bb['bb_top'] - 1
            # This -1 accounts for the width (width of 1 x1=x2)
            x2 = x1 + bb['bb_width'] - 1
            y2 = y1 + bb['bb_height'] - 1

            boxes[i, :] = [x1, y1, x2, y2]
            # Class is always pedestrian
            gt_classes[i] = 1
            seg_areas[i] = float((x2 - x1 + 1) * (y2 - y1 + 1))
            #ishards[i] = 0
            overlaps[i][1] = 1.0
            visibilities[i] = bb['visibility']
        total_boxes = len(boxes)
        gt_indexes = [a for a in range(total_boxes)]
        indexes = combinations(gt_indexes,2)
        
        bbs = []
        seg_areas = []
        visibilities = []
        classes = []
        overlaps = []
        # pdb.set_trace()
        for a_index in list(indexes):
            iou = self.bb_intersection_over_union(boxes[a_index[0]],boxes[a_index[1]])
            if iou>=.20:
                remaining_box_indexes = list(set(set(gt_indexes)-set(a_index)))
                x1 = max(boxes[a_index[0]][0],boxes[a_index[1]][0])
                y1 = max(boxes[a_index[0]][1],boxes[a_index[1]][1])
                x2 = min(boxes[a_index[0]][2],boxes[a_index[1]][2])
                y2 = min(boxes[a_index[0]][3],boxes[a_index[1]][3])
                intersected_box = np.array([x1,x2,x2,y2])
                x1_min = min(boxes[a_index[0]][0],boxes[a_index[1]][0])
                y1_min = min(boxes[a_index[0]][1],boxes[a_index[1]][1])
                x2_max = max(boxes[a_index[0]][2],boxes[a_index[1]][2])
                y2_max = max(boxes[a_index[0]][3],boxes[a_index[1]][3])
                # extra_boxes = []
                for a_remaining_box_index in remaining_box_indexes:
                    intersected_iou = self.bb_intersection_over_union(boxes[a_remaining_box_index],intersected_box)
                    if intersected_iou>=.05:
                        # extra_boxes.append(boxes[a_remaining_box_index])
                        x1_min = min(x1_min,boxes[a_remaining_box_index][0])
                        y1_min = min(y1_min,boxes[a_remaining_box_index][1])
                        x2_max = min(x2_max,boxes[a_remaining_box_index][2])
                        y2_max = min(y2_max,boxes[a_remaining_box_index][3])
                bb = np.array([x1_min,y1_min,x2_max,y2_max])
                seg_area = float((x2_max - x1_min + 1) * (y2_max - y1_min + 1))
                bbs.append(bb)
                seg_areas.append(seg_area)
                visibilities.append(1)
                classes.append(1)
                overlaps.append(np.array([0.0,1.0]))
        if len(bbs)>0:
            boxes = np.stack(bbs)
            seg_areas = np.stack(seg_areas)
            visibilities = np.stack(visibilities)
            gt_classes = np.stack(classes)
            overlaps = np.stack(overlaps)
        # pdb.set_trace()
        else:
            boxes = np.zeros((0, 4), dtype=np.int16)
            gt_classes = np.zeros((0), dtype=np.int32)
            overlaps = np.zeros((0, self.num_classes), dtype=np.float32)
            # "Seg" area for pascal is just the box area
            seg_areas = np.zeros((0), dtype=np.float32)
            #ishards = np.zeros((num_objs), dtype=np.int32)
            visibilities = np.zeros((0), dtype=np.float32)
        overlaps = scipy.sparse.csr_matrix(overlaps)
        return {'boxes': boxes,
                'gt_classes': gt_classes,
                #'gt_ishard': ishards,
                'gt_overlaps': overlaps,
                'flipped': False,
                'seg_areas': seg_areas,
                'visibilities': visibilities}

    """
    def evaluate_detections(self, all_boxes, output_dir=None, ret=False):

        if self._image_set in ["test", "all"]:
            assert ret == False, 'Evaluating on test set, can\'t return values'
            assert output_dir, 'No output dir was given!'
            self._write_results_file(all_boxes, output_dir)
        else:
            tp, fp, prec, rec, ap = self._python_eval(all_boxes)
            results_string = "True Positives: {}\nFalse Positives: {}\nPrecision: {}\nRecall: {}\nAP: {}".format(tp, fp, prec, rec, ap)
            if output_dir:
                results_file = osp.join(output_dir, 'results_scores.txt')
                print("[*] Saving results to {}".format(results_file))
                with open(results_file, "w") as rf:
                    rf.write(results_string)
                self._write_results_file(all_boxes, output_dir)
            print(results_string)

        if ret:
            return ap, rec, prec
    """
    def bb_intersection_over_union(self,boxA, boxB):
        # determine the (x, y)-coordinates of the intersection rectangle
        xA = max(boxA[0], boxB[0])
        yA = max(boxA[1], boxB[1])
        xB = min(boxA[2], boxB[2])
        yB = min(boxA[3], boxB[3])

        # compute the area of intersection rectangle
        interArea = abs(max((xB - xA, 0)) * max((yB - yA), 0))
        if interArea == 0:
            return 0
        # compute the area of both the prediction and ground-truth
        # rectangles
        boxAArea = abs((boxA[2] - boxA[0]) * (boxA[3] - boxA[1]))
        boxBArea = abs((boxB[2] - boxB[0]) * (boxB[3] - boxB[1]))

        # compute the intersection over union by taking the intersection
        # area and dividing it by the sum of prediction + ground-truth
        # areas - the interesection area
        iou = interArea / float(boxAArea + boxBArea - interArea)

        # return the intersection over union value
        return iou
    def evaluate_detections(self, all_boxes, output_dir):
        self._write_results_file(all_boxes, output_dir)
        tp, fp, prec, rec, ap = self._python_eval(all_boxes)
        print(f"AP: {ap} Prec: {prec} Rec: {rec} TP: {tp} FP: {fp}")
        return ap

    def _matlab_eval(self, all_boxes):
        pass

    def _python_eval(self, all_boxes, ovthresh=0.5):
        """Evaluates the detections (not official!!)

        all_boxes[cls][image] = N x 5 array of detections in (x1, y1, x2, y2, score)
        """

        gt_roidb = self.gt_roidb()

        # Lists for tp and fp in the format tp[cls][image]
        tp = [[[] for _ in range(len(self.image_index))]
              for _ in range(self.num_classes)]
        fp = [[[] for _ in range(len(self.image_index))]
              for _ in range(self.num_classes)]

        # sum up all positive groundtruth samples
        npos = 0

        for cls_index, cls in enumerate(self._classes):
            if cls == '__background__':
                continue
            det = all_boxes[cls_index]
            gt = []
            # keep track which groundtruth boxes have been detected already
            gt_found = []

            # Only keep gt data for right class
            for im_gt in gt_roidb:
                bbox = im_gt['boxes'][np.where(
                    np.logical_and(im_gt['gt_classes'] == cls_index,
                                   im_gt['visibilities'] >= 0.5))]
                found = np.zeros(bbox.shape[0])
                gt.append(bbox)
                gt_found.append(found)

                npos += found.shape[0]

            # Loop through all images
            for im_index, (im_det, im_gt, found) in enumerate(zip(det, gt, gt_found)):
                # Loop through dets an mark TPs and FPs

                im_tp = np.zeros(len(im_det))
                im_fp = np.zeros(len(im_det))
                for i, d in enumerate(im_det):
                    ovmax = -np.inf

                    if im_gt.size > 0:
                        # compute overlaps
                        # intersection
                        ixmin = np.maximum(im_gt[:, 0], d[0])
                        iymin = np.maximum(im_gt[:, 1], d[1])
                        ixmax = np.minimum(im_gt[:, 2], d[2])
                        iymax = np.minimum(im_gt[:, 3], d[3])
                        iw = np.maximum(ixmax - ixmin + 1., 0.)
                        ih = np.maximum(iymax - iymin + 1., 0.)
                        inters = iw * ih

                        # union
                        uni = ((d[2] - d[0] + 1.) * (d[3] - d[1] + 1.) +
                               (im_gt[:, 2] - im_gt[:, 0] + 1.) *
                               (im_gt[:, 3] - im_gt[:, 1] + 1.) - inters)

                        overlaps = inters / uni
                        ovmax = np.max(overlaps)
                        jmax = np.argmax(overlaps)

                    if ovmax > ovthresh:
                        if found[jmax] == 0:
                            im_tp[i] = 1.
                            found[jmax] = 1.
                        else:
                            im_fp[i] = 1.
                    else:
                        im_fp[i] = 1.

                tp[cls_index][im_index] = im_tp
                fp[cls_index][im_index] = im_fp

        # Flatten out tp and fp into a numpy array
        i = 0
        for cls in tp:
            for im in cls:
                if type(im) != type([]):
                    i += im.shape[0]

        tp_flat = np.zeros(i)
        fp_flat = np.zeros(i)

        i = 0
        for tp_cls, fp_cls in zip(tp, fp):
            for tp_im, fp_im in zip(tp_cls, fp_cls):
                if type(tp_im) != type([]):
                    s = tp_im.shape[0]
                    tp_flat[i:s+i] = tp_im
                    fp_flat[i:s+i] = fp_im
                    i += s

        tp = np.cumsum(tp_flat)
        fp = np.cumsum(fp_flat)
        rec = tp / float(npos)
        # avoid divide by zero in case the first detection matches a difficult
        # ground truth (probably not needed in my code but doesn't harm if left)
        prec = tp / np.maximum(tp + fp, np.finfo(np.float64).eps)
        tmp = np.maximum(tp + fp, np.finfo(np.float64).eps)

        # correct AP calculation
        # first append sentinel values at the end
        mrec = np.concatenate(([0.], rec, [1.]))
        mpre = np.concatenate(([0.], prec, [0.]))

        # compute the precision envelope
        for i in range(mpre.size - 1, 0, -1):
            mpre[i - 1] = np.maximum(mpre[i - 1], mpre[i])

        # to calculate area under PR curve, look for points
        # where X axis (recall) changes value
        i = np.where(mrec[1:] != mrec[:-1])[0]

        # and sum (\Delta recall) * prec
        ap = np.sum((mrec[i + 1] - mrec[i]) * mpre[i + 1])

        return np.max(tp), np.max(fp), prec[-1], np.max(rec), ap

    def _write_results_file(self, all_boxes, output_dir):
        """Write the detections in the format for MOT17Det sumbission

        all_boxes[cls][image] = N x 5 array of detections in (x1, y1, x2, y2, score)

        Each file contains these lines:
        <frame>, <id>, <bb_left>, <bb_top>, <bb_width>, <bb_height>, <conf>, <x>, <y>, <z>

        Files to sumbit:
        ./MOT17-01.txt
        ./MOT17-02.txt
        ./MOT17-03.txt
        ./MOT17-04.txt
        ./MOT17-05.txt
        ./MOT17-06.txt
        ./MOT17-07.txt
        ./MOT17-08.txt
        ./MOT17-09.txt
        ./MOT17-10.txt
        ./MOT17-11.txt
        ./MOT17-12.txt
        ./MOT17-13.txt
        ./MOT17-14.txt
        """

        #format_str = "{}, -1, {}, {}, {}, {}, {}, -1, -1, -1"

        files = {}
        for cls in all_boxes:
            for i, dets in enumerate(cls):
                path = self.image_path_at(i)
                img1, name = osp.split(path)
                # get image number out of name
                frame = int(name.split('.')[0])
                # smth like /train/MOT17-09-FRCNN or /train/MOT17-09
                tmp = osp.dirname(img1)
                # get the folder name of the sequence and split it
                tmp = osp.basename(tmp).split('-')
                # Now get the output name of the file
                #out = tmp[0]+'-'+tmp[1]+'.txt'
                out=tmp[0]
                outfile = osp.join(output_dir, out)

                # check if out in keys and create empty list if not
                if outfile not in files.keys():
                    files[outfile] = []

                for d in dets:
                    x1 = d[0]
                    y1 = d[1]
                    x2 = d[2]
                    y2 = d[3]
                    score = d[4]
                    files[outfile].append(
                        [frame, -1, x1+1, y1+1, x2-x1+1, y2-y1+1, score, -1, -1, -1])

        for k, v in files.items():
            #outfile = osp.join(output_dir, out)
            with open(k, "w") as of:
                writer = csv.writer(of, delimiter=',')
                for d in v:
                    writer.writerow(d)



if __name__ == '__main__':
    pass
