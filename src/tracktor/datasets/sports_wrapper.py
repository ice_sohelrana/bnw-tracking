import torch
from torch.utils.data import Dataset

from .mot_sequence import MOT17_Sequence, MOT19CVPR_Sequence, MOT17LOWFPS_Sequence
from .sports_sequence import SPORTS_Sequence
import pdb
import glob
import os
from ..config import cfg
class SPORTS_Wrapper(Dataset):
	"""A Wrapper for the MOT_Sequence class to return multiple sequences."""

	def __init__(self, split,dets,dataloader):
		"""Initliazes all subset of the dataset.

		Keyword arguments:
		split -- the split of the dataset to use
		dataloader -- args for the MOT_Sequence dataloader
		"""
		# pdb.set_trace()


		test_folders = os.path.join(cfg.DATA_DIR,'sports','test')
		# train_sequences = ['1_AUS CP']
		test_sequences = os.listdir(test_folders)
		# for a_test_folder in test_folders:
		# 	# pdb.set_trace()
		# 	test_sequences.append(a_test_folder.split("/")[-2])
		# test_sequences = ['1_AUS CP']
		# pdb.set_trace()
		if "train" == split:
			sequences = train_sequences
		elif "test" == split:
			sequences = test_sequences
		elif "all" == split:
			sequences = train_sequences + test_sequences
		# elif f"MOT17-{split}" in train_sequences + test_sequences:
		# 	sequences = [f"MOT17-{split}"]
		else:
			raise NotImplementedError("MOT split not available.")

		self._data = []
		for s in sequences:
			# if dets == '17':
			# 	self._data.append(SPORTS_Sequence(seq_name=s, dets='DPM17', **dataloader))
			# 	self._data.append(SPORTS_Sequence(seq_name=s, dets='FRCNN17', **dataloader))
			# 	self._data.append(SPORTS_Sequence(seq_name=s, dets='SDP17', **dataloader))
			# else:
			self._data.append(SPORTS_Sequence(seq_name=s, dets=dets, **dataloader))

	def __len__(self):
		return len(self._data)

	def __getitem__(self, idx):
		return self._data[idx]


# class MOT19CVPR_Wrapper(MOT17_Wrapper):
# 	"""A Wrapper for the MOT_Sequence class to return multiple sequences."""

# 	def __init__(self, split, dataloader):
# 		"""Initliazes all subset of the dataset.

# 		Keyword arguments:
# 		split -- the split of the dataset to use
# 		dataloader -- args for the MOT_Sequence dataloader
# 		"""
# 		train_sequences = ['CVPR19-01', 'CVPR19-02', 'CVPR19-03', 'CVPR19-05']
# 		test_sequences = ['CVPR19-04', 'CVPR19-06', 'CVPR19-07', 'CVPR19-08']

# 		if "train" == split:
# 			sequences = train_sequences
# 		elif "test" == split:
# 			sequences = test_sequences
# 		elif "all" == split:
# 			sequences = train_sequences + test_sequences
# 		elif f"CVPR19-{split}" in train_sequences + test_sequences:
# 			sequences = [f"CVPR19-{split}"]
# 		else:
# 			raise NotImplementedError("MOT19CVPR split not available.")

# 		self._data = []
# 		for s in sequences:
# 			self._data.append(MOT19CVPR_Sequence(seq_name=s, **dataloader))

# 	def __len__(self):
# 		return len(self._data)

# 	def __getitem__(self, idx):
# 		return self._data[idx]


# class MOT17LOWFPS_Wrapper(MOT17_Wrapper):
# 	"""A Wrapper for the MOT_Sequence class to return multiple sequences."""

# 	def __init__(self, split, dataloader):
# 		"""Initliazes all subset of the dataset.

# 		Keyword arguments:
# 		split -- the split of the dataset to use
# 		dataloader -- args for the MOT_Sequence dataloader
# 		"""

# 		sequences = ['MOT17-02', 'MOT17-04', 'MOT17-09', 'MOT17-10', 'MOT17-11']

# 		self._data = []
# 		for s in sequences:
# 			self._data.append(MOT17LOWFPS_Sequence(split=split, seq_name=s, dets='FRCNN17', **dataloader))

# 	def __len__(self):
# 		return len(self._data)

# 	def __getitem__(self, idx):
# 		return self._data[idx]
