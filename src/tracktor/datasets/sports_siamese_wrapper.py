from torch.utils.data import Dataset
import torch

from .sports_siamese import SPORTS_Siamese


class sports_Siamese_Wrapper(Dataset):
	"""A Wrapper class for MOT_Siamese.

	Wrapper class for combining different sequences into one dataset for the MOT_Siamese
	Dataset.
	"""

	def __init__(self, split, dataloader):

		train_folders = ['first_half_netball']

		self._dataloader = SPORTS_Siamese(None, split=split, **dataloader)

		for seq in train_folders:
			d = SPORTS_Siamese(seq, split=split, **dataloader)
			for sample in d.data:
				self._dataloader.data.append(sample)

	def __len__(self):
		return len(self._dataloader.data)

	def __getitem__(self, idx):
		return self._dataloader[idx]
