import time
import torch 
import torch.nn as nn
from torch.autograd import Variable
import numpy as np
import cv2 
import numpy as np
# from hungarian import Hungarian 
import csv,pdb
from torchvision import datasets, models, transforms
import glob
from PIL import Image, ImageDraw
import matplotlib.pyplot as plt
import torch
import torch.nn as nn
# from single_model import model,vel_model,grid_model, AMI_model
from torch.autograd import Variable
# from common_function import cropPIL,dis,infoDrawingRect,drawingRect
# from nms import nms
# from nms_numpy import non_max_suppression_slow
import matplotlib.pyplot as plt
from PIL import Image
import matplotlib.patches as patches
from random import randrange, uniform
from random import randint
from shutil import copyfile
# copyfile(src, dst)
import os
# plt.ion()

class video_generator(object):
	"""docstring for video_generator"""
	def __init__(self, images_path,annotation_path,videoname,save_path):
		super(video_generator, self).__init__()
		self.videoname = videoname
		self.detection_image_paths = sorted(glob.glob(images_path))
		self.save_path = save_path
		self.tracks_annotaton_path = annotation_path
	def loadPersonIDFrameNumber(self,path):
		object_list = []
		with open(path, 'r') as csvfile:
			csv_read = csv.reader(csvfile, delimiter = ',')
			for row in csv_read:
				frame_url = self.detection_image_paths[int(row[0])-1]
				person_id_frame_no = {"frame_number": int(row[0]), "person_id": row[1], "x":int(float(row[2])),"y":int(float(row[3])),"width":int(float(row[4])),"height":int(float(row[5])),"conf":float(row[6]),"url": frame_url}#,"conf":float(row[6]),"type":int(float(row[7]))} #person,person_id,frame_number
				object_list.append(person_id_frame_no)
		return object_list

	def generate(self):
		prev = '1'
		intial_prev = '1'
		iteration = 1
		image = cv2.imread(self.detection_image_paths[0])
		height,width,channels = image.shape
		object_list = self.loadPersonIDFrameNumber(self.tracks_annotaton_path)
		object_list = sorted(object_list,key=lambda x: int(x['frame_number']))
		with open(self.tracks_annotaton_path, mode='w') as csv_file:
			csv_writer = csv.writer(csv_file, delimiter=',', quotechar='"', quoting=csv.QUOTE_MINIMAL)
			for objects in object_list:
				print(objects["frame_number"])
				csv_writer.writerow([objects["frame_number"],objects["person_id"],objects["x"],objects["y"],objects["width"],objects["height"],-1,-1,-1,-1])
		out = cv2.VideoWriter(self.save_path+self.videoname+'.avi',cv2.VideoWriter_fourcc('M','J','P','G'), 6, (width,height))
		with open(self.tracks_annotaton_path, 'r') as csvfile:
			csv_read = csv.reader(csvfile, delimiter = ',')
			for row in csv_read:
				frame_url = self.detection_image_paths[int(row[0])-1]
				if iteration == 1:
					image = cv2.imread(frame_url)
					id_list = []
				if row[0] == prev:
					print(row[2])	
					if row[0] == intial_prev:
						r,g,b = randint(0, 255),randint(0, 255),randint(0, 255)
						id_list.append({"id":row[1],"r":r,"g":g,"b":b})
						cv2.putText(image, text=row[1], org=(int(float(row[2])),int(float(row[3]))),fontFace=2, fontScale=0.7, color=(255,0,0), thickness=2)
						cv2.rectangle(image, (int(float(row[2])),int(float(row[3]))), (int(float(row[2])) + int(float(row[4])), int(float(row[3])) + int(float(row[5]))), (r,g,b), 2)

					else:
						obj = row[1]
						id_index = next((index for (index, id_list) in enumerate(id_list) if id_list["id"] == obj),None)
						
						if id_index == None:
							r,g,b = randint(0, 255),randint(0, 255),randint(0, 255)
							id_list.append({"id":row[1],"r":r,"g":g,"b":b})
							cv2.putText(image, text=row[1], org=(int(float(row[2])),int(float(row[3]))),fontFace=2, fontScale=0.7, color=(255,0,0), thickness=2)
							cv2.rectangle(image, (int(float(row[2])),int(float(row[3]))), (int(float(row[2])) + int(float(row[4])), int(float(row[3])) + int(float(row[5]))), (r,g,b), 2)

						else:
							cv2.putText(image, text=row[1], org=(int(float(row[2])),int(float(row[3]))),fontFace=2, fontScale=0.7, color=(255,0,0), thickness=2)
							cv2.rectangle(image, (int(float(row[2])),int(float(row[3]))), (int(float(row[2])) + int(float(row[4])), int(float(row[3])) + int(float(row[5]))), (id_list[id_index]["r"],id_list[id_index]["g"],id_list[id_index]["b"]), 2)
				if row[0] != prev:
					# plt.imshow(image)
					new_image = cv2.resize(image,(width,height))
					out.write(new_image)
					# plt.pause(0.05)
					print(row[0])
					# plt.cla()
					image = cv2.imread(frame_url)

					obj = row[1]
					id_index = next((index for (index, id_list) in enumerate(id_list) if id_list["id"] == obj),None)
					if id_index == None:
						r,g,b = randint(0, 255),randint(0, 255),randint(0, 255)
						id_list.append({"id":row[1],"r":r,"g":g,"b":b})
						cv2.putText(image, text=row[1], org=(int(float(row[2])),int(float(row[3]))),fontFace=2, fontScale=0.7, color=(255,0,0), thickness=2)
						cv2.rectangle(image, (int(float(row[2])),int(float(row[3]))), (int(float(row[2])) + int(float(row[4])), int(float(row[3])) + int(float(row[5]))), (r,g,b), 2)
					else:
						cv2.putText(image, text=row[1], org=(int(float(row[2])),int(float(row[3]))),fontFace=2, fontScale=0.7, color=(255,0,0), thickness=2)
						cv2.rectangle(image, (int(float(row[2])),int(float(row[3]))), (int(float(row[2])) + int(float(row[4])), int(float(row[3])) + int(float(row[5]))), (id_list[id_index]["r"],id_list[id_index]["g"],id_list[id_index]["b"]), 2)
				prev = row[0]
				iteration = iteration + 1
			out.release()
			cv2.destroyAllWindows()



videoname = "43_Opp Shots"
images_path = "/media/sohel/HDD/tracking_wo_bnw/data/haritha/crop_frames/2_aus_v_nz/Q1_AUS_v_NZ/"+videoname+"/img1/*.jpg"
annotation_path = "/media/sohel/HDD/tracking_wo_bnw/data/haritha/crop_frames/tracking_output/2_aus_v_nz/Q1_AUS_v_NZ/"+videoname+".txt"
save_path = "/media/sohel/HDD/tracking_wo_bnw/"
generator = video_generator(images_path,annotation_path,videoname,save_path)
generator.generate()