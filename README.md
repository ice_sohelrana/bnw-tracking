# Tracking without bells and whistles-- An implementation for sports data

This repo provides SOTA algorithm implementation in sports tracking data(Netball and hockey).

We use tracking algorithm from the **Tracking without bells and whistles** (Philipp Bergmann, Tim Meinhardt, Laura Leal-Taixe) [https://arxiv.org/abs/1903.05625]. 

## Setup
Create an environment : ```conda env create -f environment.yml```
Activate environment ```conda activate bnw-tracking``` 

(I would suggest to make a seperate environment to avoid conflict with current version). You can visit following link to understand more about the setup. [https://github.com/phil-bergmann/tracking_wo_bnw/tree/iccv_19]


Make sure the nvcc compiler with CUDA 9.0 is working and all CUDA paths are set.  (in particular ```export CPATH=/usr/local/cuda-9.0/include``` )

Compile Faster R-CNN + FPN and Faster R-CNN: ```sh src/fpn/fpn/make.sh``` ```sh src/frcnn/frcnn/make.sh```

Keep detection and re-id weights as mentioned in ```./experiments/cfg/tracktor.yaml``` file.

## Dataset setup
Keep sports data folders in the following directory: ```./data/sports/test```

For example:
```bash
	data
		sports
			test
				9_Opp CP
				10_Opp GC Feed
				11_Opp Shots
				etc..

```


## Detection
Configuration file directory to set detection parameters:  ```./output/fpn/res101/sports_2019_train/sports_2019_train/config.yaml```


Use following command to test Faster-RCNN-FPN detection:  

```python ./experiments/scripts/test_fpn.py sports_2019_train --cuda --net res101 --dataset sports_2019_train --imdbval_name sports_2019_test --checkepoch 12```


Detection outputs will be stored inside each data folder like ```./data/sports/test/9_Opp CP/det/det.txt```


## Tracking
Configuration file directory to set tracking parameters:  ```./experiments/cfg/tracktor.yaml```


Use following command to test the tracker:  
```python ./experiments/scripts/test_tracktor.py```


Tracking output will be stored in following directory ```./output/tracktor/SPORTS19/Tracktor++```



